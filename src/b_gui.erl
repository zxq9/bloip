%%% @doc
%%% Blobby Space over IP GUI
%%%
%%% This process is responsible for creating the main GUI frame displayed to the user.
%%%
%%% Reference: http://erlang.org/doc/man/wx_object.html
%%% @end

-module(b_gui).
-vsn("0.1.0").
-author("Craig Everett <zxq9@zxq9.com>").
-copyright("Craig Everett <zxq9@zxq9.com>").
-license("GPL-3.0-or-later").

-behavior(wx_object).
-include_lib("wx/include/wx.hrl").
-include_lib("wx/include/gl.hrl").
-export([message/1]).
-export([start_link/1]).
-export([init/1, terminate/2, code_change/3,
         handle_call/3, handle_cast/2, handle_info/2, handle_event/2]).
-include("$zx_include/zx_logger.hrl").


-record(s,
        {frame    = none :: none | wx:wx_object(),
         canvas   = none :: none | wxGLCanvas:wxGLCanvas(),
         gl       = new  :: old | new,
         chat_win = none :: none | wx:wx_object(),
         type_win = none :: none | wx:wx_object(),
         chat_log = []   :: [unicode:chatdata()],
         chat_len = 0    :: non_neg_integer(),
         chat_max = 50   :: pos_integer(),
         tpin     = none :: none | pin(),
         rpin     = none :: none | pin(),
         tx       = 0.0  :: number(),
         ty       = 0.0  :: number(),
         rx       = 0.0  :: number(),
         ry       = 0.0  :: number()}).


-record(chat,
        {ts   = {0, 0, 0} :: erlang:timestamp(),
         from = ""        :: string(),
         text = ""        :: string()}).


-type state() :: #s{}.
-type pin()   :: {ClickX :: non_neg_integer(),
                  ClickY :: non_neg_integer(),
                  OrigX  :: non_neg_integer(),
                  OrigY  :: non_neg_integer()}.
-type chat()  :: #chat{}.



%%% Interface functions

message(Data) ->
    wx_object:cast(?MODULE, {message, Data}).



%%% Startup Functions

start_link(Title) ->
    wx_object:start_link({local, ?MODULE}, ?MODULE, Title, []).


init(Title) ->
    ok = log(info, "GUI starting..."),
    Wx = wx:new(),
    Frame = wxFrame:new(Wx, ?wxID_ANY, Title),
    MainSz = wxBoxSizer:new(?wxVERTICAL),
    CanvasAttributes =
        [{style, ?wxFULL_REPAINT_ON_RESIZE},
         {attribList,
          [?WX_GL_RGBA,
           ?WX_GL_MIN_RED,        8,
           ?WX_GL_MIN_GREEN,      8,
           ?WX_GL_MIN_BLUE,       8,
           ?WX_GL_DEPTH_SIZE,     24,
           ?WX_GL_DOUBLEBUFFER,
           ?WX_GL_SAMPLES,        4,
           0]}],
    Canvas = wxGLCanvas:new(Frame, CanvasAttributes),
    InsetSz = wxBoxSizer:new(?wxVERTICAL),
    ChatSz = wxBoxSizer:new(?wxHORIZONTAL),
    ok = wxGLCanvas:connect(Canvas, paint),
    ok = wxGLCanvas:connect(Canvas, left_down),
    ok = wxGLCanvas:connect(Canvas, left_up),
    ok = wxGLCanvas:connect(Canvas, right_down),
    ok = wxGLCanvas:connect(Canvas, right_up),
    ok = wxGLCanvas:connect(Canvas, motion),
    Mono = wxFont:new(8, ?wxMODERN, ?wxNORMAL, ?wxNORMAL, [{face, "Monospace"}]),
    Chat = wxTextCtrl:new(Frame, ?wxID_ANY, [{style,    ?wxDEFAULT
                                                    bor ?wxVSCROLL
                                                    bor ?wxALWAYS_SHOW_SB
                                                    bor ?wxTE_MULTILINE
                                                    bor ?wxTE_READONLY
                                                    bor ?wxBG_STYLE_TRANSPARENT}]),
    Type = wxTextCtrl:new(Frame, ?wxID_ANY),
    true = wxTextCtrl:setFont(Chat, Mono),
    true = wxTextCtrl:setFont(Type, Mono),
    wxTextCtrl:setBackgroundColour(Chat, {10, 10, 10, 0}),
    wxTextCtrl:setForegroundColour(Chat, {225, 225, 225, 255}),
    wxTextCtrl:setBackgroundColour(Type, {10, 10, 10, 0}),
    wxTextCtrl:setForegroundColour(Type, {225, 225, 225, 255}),
    _ = wxBoxSizer:add(MainSz, Canvas, [{flag, ?wxEXPAND}, {proportion, 1}]),
    _ = wxBoxSizer:addStretchSpacer(InsetSz, [{prop, 3}]),
    _ = wxBoxSizer:addStretchSpacer(ChatSz, [{prop, 1}]),
    _ = wxBoxSizer:add(ChatSz, Chat, [{flag, ?wxEXPAND}, {proportion, 10}]),
    _ = wxBoxSizer:addStretchSpacer(ChatSz, [{prop, 1}]),
    _ = wxBoxSizer:add(InsetSz, ChatSz, [{flag, ?wxEXPAND}, {proportion, 1}]),
    _ = wxBoxSizer:add(InsetSz, Type, [{flag, ?wxEXPAND}, {proportion, 0}]),
    ok = wxFrame:setSizer(Frame, MainSz),
    ok = wxGLCanvas:setSizer(Canvas, InsetSz),
    ok = wxSizer:layout(MainSz),
    ok = wxFrame:setClientSize(Frame, 800, 600),
    ok = wxFrame:connect(Frame, close_window),
    ok = wxFrame:center(Frame),
    true = wxFrame:show(Frame),
    GL =
        case erlang:system_info(otp_release) >= "24" of
            true ->
                true = wxGLCanvas:setCurrent(Canvas, wxGLContext:new(Canvas)),
                new;
            false ->
                ok = wxGLCanvas:setCurrent(Canvas),
                old
        end,
    ok = gl:enable(?GL_DEPTH_TEST),
    ok = gl:depthFunc(?GL_LESS),
    ok = gl:enable(?GL_CULL_FACE),
    ok = gl:enable(?GL_MULTISAMPLE),
    ok = gl:cullFace(?GL_BACK),
    State = #s{frame = Frame, canvas = Canvas, gl = GL,
               chat_win = Chat, type_win = Type},
    ok = draw(State),
    {Frame, State}.


draw(#s{frame = Frame, canvas = Canvas, gl = GL, tx = TX, ty = TY, rx = RX, ry = RY}) ->
    ok = gl:clearColor(0.1, 0.1, 0.2, 1.0),
    ok = gl:color3f(1.0, 1.0, 1.0),
    {W, H} = wxFrame:getClientSize(Frame),
    ok = gl:viewport(0, 0, W, H),
    ok = gl:matrixMode(?GL_PROJECTION),
    ok = gl:loadIdentity(),
%   ok = gl:frustum(-3, 3, -5 * H / W, 5 * H / W, 1, 40),
    ok = gl:ortho(-3.0, 3.0, -3.0 * H / W, 3.0 * H / W, -20.0, 20.0),
    ok = gl:matrixMode(?GL_MODELVIEW),
    ok = gl:loadIdentity(),
    ok = gl:clear(?GL_COLOR_BUFFER_BIT bor ?GL_DEPTH_BUFFER_BIT),
    ok = gl:translatef(TX, TY, -3.0),
    ok = gl:rotatef(RY, 1.0, 0.0, 0.0),
    ok = gl:rotatef(RX, 0.0, 1.0, 0.0),
    ok = gl:'begin'(?GL_LINES),
    ok = grid(-2.5, 2.5),
    ok = gl:'end'(),
    ok = gl:'begin'(?GL_TRIANGLE_STRIP),
    T =
        fun({{R, G, B}, {X, Y, Z}}) ->
            ok = gl:color3f(R, G, B),
            ok = gl:vertex3f(X, Y, Z)
        end,
    Data =
        [{{1.0, 0.0, 1.0}, { 0.0,  1.0,  0.0}},
         {{1.0, 0.0, 0.0}, {-1.0,  0.0,  1.0}},
         {{0.0, 1.0, 0.0}, { 1.0,  0.0,  1.0}},
         {{0.0, 0.0, 1.0}, { 0.0,  0.0, -1.0}},
         {{1.0, 0.0, 1.0}, { 0.0,  1.0,  0.0}},
         {{1.0, 0.0, 0.0}, {-1.0,  0.0,  1.0}}],
    ok = lists:foreach(T, Data),
    ok = gl:'end'(),
    case GL of
        new ->
            true = wxGLCanvas:swapBuffers(Canvas),
            ok;
        old ->
            wxGLCanvas:swapBuffers(Canvas)
    end.

grid(N, Max) when N =< Max ->
    ok = gl:vertex3f(   N,  0.0,  2.5),
    ok = gl:vertex3f(   N,  0.0, -2.5),
    ok = gl:vertex3f( 2.5,  0.0,    N),
    ok = gl:vertex3f(-2.5,  0.0,    N),
    ok = gl:vertex3f( 0.0,    N,  2.5),
    ok = gl:vertex3f( 0.0,    N, -2.5),
    ok = gl:vertex3f( 0.0,  2.5,    N),
    ok = gl:vertex3f( 0.0, -2.5,    N),
    ok = gl:vertex3f( 2.5,    N,  0.0),
    ok = gl:vertex3f(-2.5,    N,  0.0),
    ok = gl:vertex3f(   N,  2.5,  0.0),
    ok = gl:vertex3f(   N, -2.5,  0.0),
    grid(N + 0.25, Max);
grid(_, _) ->
    ok.


-spec handle_call(Message, From, State) -> Result
    when Message  :: term(),
         From     :: {pid(), reference()},
         State    :: state(),
         Result   :: {reply, Response, NewState}
                   | {noreply, State},
         Response :: ok
                   | {error, {listening, inet:port_number()}},
         NewState :: state().

handle_call(Unexpected, From, State) ->
    ok = log(warning, "Unexpected call from ~tp: ~tp~n", [From, Unexpected]),
    {noreply, State}.


-spec handle_cast(Message, State) -> {noreply, NewState}
    when Message  :: term(),
         State    :: state(),
         NewState :: state().
%% @private
%% The gen_server:handle_cast/2 callback.
%% See: http://erlang.org/doc/man/gen_server.html#Module:handle_cast-2

handle_cast({message, Data}, State) ->
    NewState = do_message(Data, State),
    {noreply, NewState};
handle_cast(Unexpected, State) ->
    ok = tell(warning, "Unexpected cast: ~tp~n", [Unexpected]),
    {noreply, State}.


-spec handle_info(Message, State) -> {noreply, NewState}
    when Message  :: term(),
         State    :: state(),
         NewState :: state().
%% @private
%% The gen_server:handle_info/2 callback.
%% See: http://erlang.org/doc/man/gen_server.html#Module:handle_info-2

handle_info(Unexpected, State) ->
    ok = tell(warning, "Unexpected info: ~tp~n", [Unexpected]),
    {noreply, State}.


-spec handle_event(Event, State) -> {noreply, NewState}
    when Event    :: term(),
         State    :: state(),
         NewState :: state().
%% @private
%% The wx_object:handle_event/2 callback.
%% See: http://erlang.org/doc/man/gen_server.html#Module:handle_info-2

handle_event(#wx{event = #wxClose{}}, State = #s{frame = Frame}) ->
    ok = b_con:stop(),
    ok = wxWindow:destroy(Frame),
    {noreply, State};
handle_event(#wx{event = #wxPaint{}}, State) ->
    ok = draw(State),
    {noreply, State};
handle_event(#wx{event = #wxMouse{type = motion, leftDown = true, x = X, y = Y}},
             State) ->
    NewState = do_traverse(X, Y, State),
    ok = draw(NewState),
    {noreply, NewState};
handle_event(#wx{event = #wxMouse{type = motion, leftDown = false}},
             State = #s{tpin = {_, _, _, _}}) ->
    {noreply, State#s{tpin = none}};
handle_event(#wx{event = #wxMouse{type = motion, rightDown = true, x = X, y = Y}},
             State) ->
    NewState = do_rotate(X, Y, State),
    ok = draw(NewState),
    {noreply, NewState};
handle_event(#wx{event = #wxMouse{type = motion, rightDown = false}},
             State = #s{rpin = {_, _, _, _}}) ->
    {noreply, State#s{rpin = none}};
handle_event(#wx{event = #wxMouse{}}, State) ->
    {noreply, State};
handle_event(Event, State) ->
    ok = tell(info, "Unexpected event ~tp State: ~tp~n", [Event, State]),
    {noreply, State}.



code_change(_, State, _) ->
    {ok, State}.


terminate(Reason, State) ->
    ok = log(info, "Reason: ~tp, State: ~tp", [Reason, State]),
    wx:destroy().



do_message(#chat{ts = TS, from = From, text = Text},
           State = #s{chat_win = Win, chat_log = Log, chat_len = L, chat_max = X}) ->
    F = "\n[~4..0w-~2..0w-~2..0wT~2..0w:~2..0w:~2..0w] ~ts: ~ts",
    {{Year, Month, Day}, {Hour, Minute, Second}} = calendar:now_to_datetime(TS),
    T = io_lib:format(F, [Year, Month, Day, Hour, Minute, Second, From, Text]),
    RLog = lists:reverse([T | Log]),
    {NewLog, NewL} =
        case L >= X of
            true  -> {tl(RLog), X};
            false -> {RLog, L + 1}
        end,
    ok = wxTextCtrl:setValue(Win, unicode:characters_to_list(NewLog)),
    ok = wxTextCtrl:setInsertionPointEnd(Win),
    State#s{chat_log = lists:reverse(NewLog), chat_len = NewL}.


do_traverse(X, Y, State = #s{tpin = none, tx = TX, ty = TY}) ->
    TPIN = {X, Y, TX, TY},
    State#s{tpin = TPIN};
do_traverse(X, Y, State = #s{tpin = {PX, PY, OrigX, OrigY}}) ->
    TX = OrigX - ((PX - X) / 40),
    TY = OrigY + ((PY - Y) / 40),
    State#s{tx = TX, ty = TY}.


do_rotate(X, Y, State = #s{rpin = none, rx = RX, ry = RY}) ->
    RPIN = {X, Y, RX, RY},
    State#s{rpin = RPIN};
do_rotate(X, Y, State = #s{rpin = {PX, PY, OrigX, OrigY}}) ->
    RX = OrigX + ((PX - X) / -2),
    RY = OrigY + ((PY - Y) / -2),
    State#s{rx = RX, ry = RY}.
